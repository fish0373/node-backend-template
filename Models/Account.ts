import { DataTypes, Model, UUIDV4, Optional, ModelAttributes } from 'sequelize';
import bcrypt from 'bcrypt';
import { ERROR, NeError } from '../NeError';

export interface AccountAttributes {
  accountId: string;
  account: string;
  password?: string;
  email: string;
  verified?: boolean;

}

interface AccountCreationAttributes extends Optional<AccountAttributes, 'accountId'> {
}

export class Account extends Model<AccountAttributes, AccountCreationAttributes> implements AccountAttributes {
  public accountId!: string;
  public account!: string;
  public password!: string;
  public email!: string;
  public verified!: boolean;

  public readonly createAt!: Date;
  public readonly updatedAt!: Date;

  /** 檢查密碼是否與資料庫的一致 */
  isValidPassword(password: string): boolean {
    if (bcrypt.compareSync(password, this.password)) return true;
    throw new NeError(ERROR.ACCOUNT_INVALID)
  }
  /** 轉JSON時去除密碼 */
  toJSON() {
    const values = Object.assign({}, this.get());
    delete values['password'];
    return values;
  }
}

export const AccountSchema: ModelAttributes = {
  accountId: {
    type: DataTypes.CHAR(36),
    defaultValue: UUIDV4,
    primaryKey: true,
    allowNull: false,
  },
  account: {
    type: DataTypes.STRING,
    unique: true,
    allowNull: false,
    validate: {
      len: {
        args: [5, 100],
        msg: 'account length must more than 5 words'
      }
    }
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      len: {
        args: [8, 100],
        msg: 'password length must more than 8 words'
      }
    }
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      isEmail: true,
    }
  },
  verified: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  }
};


export default Account;