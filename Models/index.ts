import { Sequelize } from 'sequelize';
import { sequelizeOptions } from '../config';
import { Account, AccountSchema } from './Account'
import bcrypt from 'bcrypt';

export const sequelize: Sequelize = new Sequelize(sequelizeOptions);

export const sequelizeInit = async () => {
  await sequelize.sync();
}

Account.init(AccountSchema, {
  sequelize,
  hooks: {
    beforeCreate: (account: Account) => {
      const salt = bcrypt.genSaltSync();
      account.password = bcrypt.hashSync(account.password, salt);
    },
  },
});


export {
  Account
}