import express from 'express';
import asyncHandler from 'express-async-handler';
import bodyParser from 'body-parser';
import { MySQLSession, SessionChecker } from './Plugins/MySQLSession';
import { NeError, ERROR, ErrorHandler } from './NeError';
import { sequelizeInit } from './Models';
import { Account } from './Models';
import { Permission } from './Plugins/Permission';

import AccountRouter from './Routers/Account';
import PermissionRouter from './Routers/Permission';

declare global {
  namespace Express {
    interface Session {
      account: Account
    }
  }
};

const app = express();

const accessRouteList = [
  '/account/login',
  '/account/register',
  '/account/verify'
];
app.use(MySQLSession());
app.use(SessionChecker('account', accessRouteList, new NeError(ERROR.UNAUTHORIZED)));

sequelizeInit();
app.use(bodyParser.json());
app.use('/account', AccountRouter);
app.use('/permission', PermissionRouter)
app.get('/', asyncHandler(async (req, res) => {

  const r = await Permission.enforce(req.session?.account.accountId, 'permission', 'read');
  res.send(r);
}));

app.use(ErrorHandler());

app.listen(3000, () => {
  console.log('listen on : ', 3000);
});