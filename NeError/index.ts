import { NextFunction, Request, Response } from "express";

type error = [number, string];
export const ERROR: {
  UNAUTHORIZED: error,
  MISSING_PARAMETER: error,
  ACCOUNT_INVALID: error,
  PERMISSION_INVALID: error,
  DUPLICATE_VALUES: error,
  PAGE_NOTFOUND: error,
  TOKEN_EXPIRED: error,
  RESOURCE_NOT_EXISTS: error,
} = {
  UNAUTHORIZED: [401, 'unauthorized'],
  MISSING_PARAMETER: [400, 'missing parameter'],
  ACCOUNT_INVALID: [403, 'cannot found invalid account or password'],
  PERMISSION_INVALID: [403, 'permission denied'],
  DUPLICATE_VALUES: [400, 'duplicate values'],
  PAGE_NOTFOUND: [404, 'page not found'],
  TOKEN_EXPIRED: [404, 'token is expired'],
  RESOURCE_NOT_EXISTS: [404, 'resource is not exist']
}

export class NeError extends Error {
  statusCode = 0;
  constructor([statusCode, errorMsg]: error) {
    super(errorMsg);
    this.statusCode = statusCode
  }
}

export const ErrorHandler = () => {
  return (err: Error & NeError, req: Request, res: Response, next: NextFunction) => {
    const error = ErrorFormatter(err);
    res.status(error.statusCode || 500).send({ msg: error.message });
  };
}

function ErrorFormatter(error: Error & NeError): NeError {
  const statusCode = 500;
  let errorMsg = error.message;
  if (error.name === 'SequelizeUniqueConstraintError') errorMsg = 'resource is not exist';
  if (error.name === 'SequelizeValidationError') errorMsg = 'missing parameter or parameter validation failed';

  const formatError = new NeError([statusCode, errorMsg])
  return formatError;
}