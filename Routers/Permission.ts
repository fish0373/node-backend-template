import { Permission } from '../Plugins/Permission';
import express from 'express';
import asyncHandler from 'express-async-handler';
import { ERROR, NeError } from '../NeError';
const router = express.Router();

type action = 'read' | 'write'


router.get('/', asyncHandler(async (req, res) => {
  if (!req.session?.account.accountId) throw new NeError(ERROR.UNAUTHORIZED);
  const r = await Permission.getPermissionsForUser(req.session?.account.accountId);
  res.send(r);
}))

router.use(asyncHandler(async (req, res, next) => {
  if (!(await Permission.enforce(req.session?.account.accountId, 'permission', 'write'))) throw new NeError(ERROR.PERMISSION_INVALID);
  if (req.body.action && !['read', 'write', '*'].includes(req.body.action)) throw new NeError(ERROR.MISSING_PARAMETER);
  next();
}));

// 取得所有群組規則
router.get('/role', asyncHandler(async (req, res) => {
  const gPolicies = await Permission.getGroupingPolicy();
  res.send(gPolicies);
}));

// 新增一筆自訂群組規則
router.post('/role', asyncHandler(async (req, res) => {
  const { groupRoleName, target, action }: { groupRoleName: string, target: string, action: action } = req.body;
  if (!groupRoleName || !target || !action) throw new NeError(ERROR.MISSING_PARAMETER);

  if (!await Permission.addPolicy(groupRoleName, target, action)) throw new NeError(ERROR.DUPLICATE_VALUES);
  res.send({ msg: 'ok' });
}));

// 新增使用者到群組規則{role}
router.post('/:role', asyncHandler(async (req, res) => {
  const { accountId }: { accountId: string } = req.body;
  const { role } = req.params;
  if (!accountId || !role) throw new NeError(ERROR.MISSING_PARAMETER);

  await Permission.addGroupingPolicy(accountId, role);
  res.send({ msg: 'ok' });
}));



export default router;