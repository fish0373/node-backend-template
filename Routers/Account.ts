import express from 'express';
import asyncHandler from 'express-async-handler';
import jwt from 'jsonwebtoken';
import moment from 'moment';
import { forEach } from 'lodash';
import { Op, WhereAttributeHash } from 'sequelize';
import { Account, AccountSchema, AccountAttributes } from '../Models/Account';
import { ERROR, NeError } from '../NeError';
import { Permission } from '../Plugins/Permission';
import QueryParser from '../Plugins/QueryParser';
import mailer from '../Plugins/Mailer';
import { domain, protocol, siteName, jwtPrivateKey, jwtExpireTime } from '../config';
import { sequelize } from '../Models';
const router = express.Router();

router.use(QueryParser(AccountSchema, { allowMethods: ['GET'] }));

// 寄送驗證信
async function sendValidateEmail(account: Account) {
  if (!account.email) return '';
  const jwtText = jwt.sign({
    account: account.account,
    redirect: 'https://google.com',
  }, jwtPrivateKey, { expiresIn: jwtExpireTime });
  try {
    await mailer.sendMail({
      to: account.email,
      subject: `${siteName}帳號註冊驗證`,
      html: `
    <h3>${siteName}帳號註冊驗證</h3>
    <div>請使用以下驗證連結完成註冊程序</div>
    <href>${protocol}://${domain}/account/verify?a=${jwtText}</href>
    `,
    })
  } catch (error) {
    console.log(error);
  }
}

// 註冊帳號
router.post('/register', asyncHandler(async (req, res) => {
  const { account, password, email } = req.body
  const t = await sequelize.transaction();
  try {
    const newAccount = await Account.create({
      account,
      password,
      email,
    }, { transaction: t });
    if (!newAccount) throw new NeError(ERROR.ACCOUNT_INVALID);
    await sendValidateEmail(newAccount);
    await t.commit();
    res.send(newAccount);
  } catch (error) {
    await t.rollback()
    throw error;
  }
}));

// 驗證帳號
router.get('/verify', asyncHandler(async (req, res) => {
  const { a } = req.query
  if (!a) throw new NeError(ERROR.PAGE_NOTFOUND);

  const jwtAccount: string | { [key: string]: any } | null = jwt.decode(a.toString());
  if (!jwtAccount || typeof jwtAccount === 'string' || jwtAccount.exp < moment().unix()) {
    throw new NeError(ERROR.TOKEN_EXPIRED);
  };

  const account = await Account.findOne({ where: { account: jwtAccount.account, verified: false } })
  if (!account) throw new NeError(ERROR.PAGE_NOTFOUND);
  await account.update({ verified: true })
  if (jwtAccount.redirect) res.redirect(jwtAccount.redirect)
  else res.send({ msg: 'ok' });
}));

// 重新寄送驗證信
router.post('/resendMail', asyncHandler(async (req, res) => {
  const { accountId } = req.body;
  if (!accountId) throw new NeError(ERROR.MISSING_PARAMETER);
  const account = await Account.findOne({ where: { accountId, verified: false } });
  if (!account) throw new NeError(ERROR.RESOURCE_NOT_EXISTS)
  await sendValidateEmail(account);
  res.send({ msg: 'ok' })
}))

// 登入
router.post('/login', asyncHandler(async (req, res) => {
  const { account, password } = req.body;
  const loginAcc = await Account.findOne({ where: { account } });
  if (!loginAcc) throw new NeError(ERROR.ACCOUNT_INVALID);
  loginAcc.isValidPassword(password);
  if (req.session) req.session.account = loginAcc;
  res.send(loginAcc.verified ? loginAcc : { msg: 'please verify this account' });
}));

// 登出
router.get('/logout', asyncHandler(async (req, res) => {
  if (req.session) req.session.destroy(() => { });
  res.send({ msg: 'ok' });
}));

// 取得帳號清單
router.get('/', asyncHandler(async (req, res) => {
  if (!await Permission.enforce(req.session?.account.accountId, 'account_manager', 'read')) throw new NeError(ERROR.PERMISSION_INVALID)
  const { limit, skip, like, equal } = req.formatQuery;

  const where: WhereAttributeHash<Account> = {};
  if (like) {
    if (like.account) where['account'] = { [Op.like]: `%${like.account}%` };
  }

  const fieldList: Array<keyof AccountAttributes> = ['account', 'accountId']
  if (equal) {
    forEach(fieldList, (field) => {
      if (equal[field]) where[field] = { [Op.eq]: equal[field] };
    });
  }
  const accountList = await Account.findAll({
    limit, offset: skip,
    where
  })
  res.send(accountList);
}))

export default router