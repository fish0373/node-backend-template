import { NextFunction, Request, Response } from 'express';
import { each, find, map, toArray } from 'lodash';
import { Model, ModelAttributeColumnOptions, ModelAttributes, Optional } from 'sequelize';

enum QueryRowAttribute { like = 'like', equal = 'equal' };
enum QuantityAttribute { limit = 'limit', skip = 'skip' };
declare global {
  namespace Express {
    interface Request {
      formatQuery:
      {
        [key in QueryRowAttribute]?: { [name in keyof Optional<any, any>]?: string | number | boolean };
      } & {
        [key in QuantityAttribute]?: number
      }
    }
  }
}

type QueryRow = [string, string | number | boolean];

function toQueryRow(value: string): QueryRow | undefined {
  const row = value.split(',', 2) as [string, string];
  if (row.length === 2) return row;
  if (row.length === 1) return [row[0], ''];
}

/**
 * @param allowMethods 允許的method
 * @param keepFailed 是否保留轉換失敗的
 */
type options = {
  allowMethods?: string[],
  keepFailed?: false,
}

/**
 * 生成一個用於 Express.js 的query轉換器
 * 生成結果會存於 `req.formatQuery`
 * @param targetSchema 參照的表單
 * @param options 選項
 */
export const QueryParser = (
  targetSchema: ModelAttributes<Model<any, any>, any>,
  options: options = { allowMethods: ['GET', 'POST'] }
) => {
  return ((req: Request, res: Response, next: NextFunction) => {
    if (options.allowMethods?.includes(req.method)) {
      const { limit, skip } = req.query;
      req.formatQuery = { limit: 100, skip: 0, };
      if (typeof limit === 'string') req.formatQuery.limit = parseInt(limit, 10);
      if (typeof skip === 'string') req.formatQuery.skip = parseInt(skip, 10);
      const schema = Object.values(targetSchema) as Array<ModelAttributeColumnOptions>;
      toArray(QueryRowAttribute).forEach((key) => {
        const values = req.query[key];
        if (values) {
          let rawList: Array<QueryRow | undefined> = [];
          if (Array.isArray(values)) rawList = map(values, (e) => toQueryRow(e.toString()));
          else if (typeof values === 'string') rawList = [toQueryRow(values)];

          let queryObject: { [key: string]: string | number | boolean } = {};
          each(rawList, (row) => {
            if (!row) return;
            const attributeColumn = find(schema, (e) => e.field === row[0]);
            if (!attributeColumn) return;

            if (attributeColumn.type.constructor.name === 'STRING') { queryObject[row[0]] = String(row[1]); return; }
            if (attributeColumn.type.constructor.name === 'NUMBER') { queryObject[row[0]] = Number(row[1]); return; }
            if (attributeColumn.type.constructor.name === 'BOOLEAN') { queryObject[row[0]] = Boolean(row[1]); return; }
            if (options.keepFailed) { queryObject[row[0]] = row[1]; return; }

            throw new Error('fail to process query string');
          });
          req.formatQuery[key] = queryObject;
        }
      })
    }

    next();
  })
}

export default QueryParser;