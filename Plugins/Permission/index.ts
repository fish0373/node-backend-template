import { SequelizeAdapter } from '../casbin-sequelize-adapter';
import { Enforcer, newEnforcer } from 'casbin'
import { Account } from '../../Models';

export let Permission: Enforcer
(async () => {
  const a = await SequelizeAdapter.newAdapter({
    host: '10.37.129.254',
    username: 'rong',
    password: '!QAZ2wsx',
    database: 'casbin',
    dialect: 'mysql',
    logging: false,
  });
  Permission = await newEnforcer('Plugins/Permission/rbac_model.conf', a);

  await Permission.addPolicy('admin', '*', '*');

  let admin = await Account.findOne({ where: { account: 'admin' } });
  if (!admin) await Account.create({ account: 'admin', password: 'password', email: 'nene@email.com', verified: true });
  if (admin) await Permission.addGroupingPolicy(admin.accountId, 'admin');
  await Permission.addPolicy('account_manager', 'account', '*')

  // e.enforce('alice', 'data1', 'read');
  // await e.savePolicy();
})()
export default Permission;
