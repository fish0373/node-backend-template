import nodemailer from 'nodemailer';
import { nodemailerOptions } from '../../config';

export const mailer = nodemailer.createTransport(nodemailerOptions);

export default mailer;