import { DataTypes, Model, Optional, ModelAttributes } from 'sequelize';

interface CasbinRuleAttributes {
  ptype: string;
  v0: string;
  v1: string;
  v2: string;
  v3: string;
  v4: string;
  v5: string;
}

interface CasbinRuleCreationAttributes extends Optional<CasbinRuleAttributes, 'ptype'> { }

export class CasbinRule extends Model<CasbinRuleAttributes, CasbinRuleCreationAttributes> implements CasbinRuleAttributes {
  public ptype!: string;
  public v0!: string;
  public v1!: string;
  public v2!: string;
  public v3!: string;
  public v4!: string;
  public v5!: string;
}

export const CasbinRuleSchema: ModelAttributes = {
  ptype: {
    type: DataTypes.STRING,
  },
  v0: {
    type: DataTypes.STRING,
  },
  v1: {
    type: DataTypes.STRING,
  },
  v2: {
    type: DataTypes.STRING,
  },
  v3: {
    type: DataTypes.STRING,
  },
  v4: {
    type: DataTypes.STRING,
  },
  v5: {
    type: DataTypes.STRING,
  },
}

export default CasbinRule;