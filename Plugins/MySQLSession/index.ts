import { NextFunction, Request, RequestHandler, Response } from 'express';
import expressSession from 'express-session'
const MySQLStore = require('express-mysql-session')(expressSession);
import { mySQLStoreOptions } from '../../config';

const sessionStore = new MySQLStore(mySQLStoreOptions)

export const MySQLSession = (secret?: string) => {
  return expressSession({
    secret: secret || 'NeSecret',
    saveUninitialized: false,
    resave: false,
    store: sessionStore,
  })
}

/**
 * SessionChecker
 * @param checkVariables 要檢查的變數
 * @param ignorePath     略過檢查的路由(可選)
 * @param customERROR    自訂錯誤(可選)
 */
export const SessionChecker = (checkVariables: string, ignorePath?: Array<string>, customERROR?: Error) => {
  return ((req: Request, res: Response, next: NextFunction) => {
    if ((ignorePath && ignorePath.findIndex((route) => route === req.path) >= 0)
      || req.session![checkVariables]
    ) {
      next();
    } else {
      if (customERROR) throw customERROR;
      throw new Error('unauthorized')
    }
  })
}