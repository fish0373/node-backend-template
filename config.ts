import MySQLStore from "express-mysql-session"
import { Options } from "sequelize/types"
import { TransportOptions } from 'nodemailer';
import JSONTransport from 'nodemailer/lib/json-transport';
import SendmailTransport from 'nodemailer/lib/sendmail-transport';
import SMTPPool from 'nodemailer/lib/smtp-pool';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import StreamTransport from 'nodemailer/lib/stream-transport';


const
  dbHost = '10.37.129.254',
  dbUser = 'rong',
  dbPwd = '!QAZ2wsx',
  sessionDB = 'sessions',
  dataDB = 'ne_default';

export const
  siteName = '捏捏',
  domain = 'localhost:3000',
  ssh = false,
  protocol = ssh ? 'https' : 'http',
  jwtPrivateKey = 'privateKey',
  jwtExpireTime = '60m';

export const mySQLStoreOptions: MySQLStore.Options = {
  host: dbHost,
  port: 3306,
  user: dbUser,
  password: dbPwd,
  database: sessionDB,
  schema: {
    tableName: 'sessions',
    columnNames: {
      session_id: 'session_id',
      expires: 'expires',
      data: 'data'
    }
  }
}

export const nodemailerOptions:
  SMTPTransport.Options |
  SMTPPool.Options |
  SendmailTransport.Options |
  StreamTransport.Options |
  JSONTransport.Options |
  TransportOptions =
{
  service: 'gmail',
  auth: {
    user: '',
    pass: '',
  }
};

export const sequelizeOptions: Options = {
  host: dbHost,
  dialect: 'mysql',
  username: dbUser,
  password: dbPwd,
  database: dataDB,
  define: {
    freezeTableName: true
  },
  logging: false,
}